package com.siriphonpha.application1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.core.os.bundleOf
import com.siriphonpha.application1.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var buttonHelloBinding:Button = findViewById(R.id.btn_hello)
        buttonHelloBinding.setOnClickListener {
            setContentView(R.layout.hello)
        }

    }

}